from datetime import datetime

from flask import Flask, render_template, redirect, url_for, request
from flask_login import LoginManager, UserMixin, login_user, current_user
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SECRET_KEY'] = '12e345k6789z015'
db = SQLAlchemy(app)
login = LoginManager(app)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username


class Pet(db.Model):
    rate_of_hunger_decline = 1
    rate_of_hygiene_decline = 1
    rate_of_play_decline = 1
    type = None

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    type = db.Column(db.String(120), nullable=False)
    hunger = db.Column(db.Integer, nullable=False)
    hygiene = db.Column(db.Integer, nullable=False)
    play = db.Column(db.Integer, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    start =db.Column(db.Integer, nullable=False)

    def needs_increase(self, time_interval):
        self.hunger = self.hunger + int(time_interval*self.rate_of_hunger_decline)
        self.hygiene = self.hygiene + int(time_interval * self.rate_of_hygiene_decline)
        self.play = self.play + int(time_interval*self.rate_of_play_decline)

    def getAnimal(self):
        if type == 'cat':
            return Cat(self)
        elif type == 'dog':
            return Dog(self)
        else:
            return self

class Cat(Pet):
    rate_of_hunger_decline = 1/48
    rate_of_hygiene_decline = 1/240
    rate_of_play_decline = 1/96
    type = 'cat'


class Dog(Pet):
    rate_of_hunger_decline = 1/40
    rate_of_hygiene_decline = 1/144
    rate_of_play_decline = 1/72
    type = 'dog'


class MyForm(FlaskForm):
    login = StringField('name', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

    def validate(self):
        is_valid = super().validate()
        return is_valid


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route("/")
def index():
    return render_template('index.html')


@app.route("/login", methods=['GET', 'POST'])
def hello():
    f = MyForm()
    is_valid = f.validate_on_submit()
    if is_valid:
        try:
            user = User.query.filter_by(username=f.login.data).first()
        except:
            return render_template("login.html", form=f, error_login='blad logowania')
        if user == None or user.password != f.password.data:
            return render_template("login.html", form=f, error_login='blad logowania')
        login_user(user)
        return redirect(url_for('pets_list'))
    return render_template("login.html", form=f, error_login='')


@app.route("/pets_list")
def pets_list():
    pets = Pet.query.filter_by(owner_id=current_user.id).all()
    pets_counter = len(pets)
    return render_template('pets_list.html', pets=pets, pet_counter=pets_counter)


@app.route('/<int:id>', methods=['GET', 'POST'])
def pet(id):
    my_pet = Pet.query.filter_by(id=id).first()
    last_visit = datetime.strptime(my_pet.start, '%Y-%m-%d %H:%M:%S.%f')
    current_visit = datetime.now()
    interval = current_visit - last_visit

    my_pet.getAnimal().needs_increase(interval.total_seconds())

    if request.method == "POST":
        if request.form['type'] == 'feed':
            my_pet.hunger -= 3
        if request.form['type'] == 'play':
            my_pet.play -= 3
        if request.form['type'] == 'bath':
            my_pet.hygiene -= 3

    my_pet.start = current_visit
    db.session.commit()
    return render_template('pet.html', pet=my_pet)


if __name__ == '__main__':
    app.run()

